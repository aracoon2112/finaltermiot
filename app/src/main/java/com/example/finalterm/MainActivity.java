package com.example.finalterm;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;


import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;
import org.json.*;
import java.nio.charset.Charset;

public class MainActivity extends AppCompatActivity {
    ImageView img;
    MQTTHelper mqttHelper;
    GraphView graph1, graph2;
    LineGraphSeries<DataPoint> series1 = new LineGraphSeries<>(new DataPoint[]{ new DataPoint(0, 0),});;
    LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>(new DataPoint[]{ new DataPoint(0, 0),});
    TextView compassText,stepText;
    private double heading =0.0f;
    private int    step = 0;
    private  void updateDashboard(double orientation , int new_step){
        Animation animation= new RotateAnimation(-(float)heading,-(float)orientation,Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        animation.setDuration(500);
        animation.setRepeatCount(0);
        animation.setFillAfter(true);
        img.startAnimation(animation);
        heading=orientation;
        compassText.setText(String.valueOf(orientation)+'°');
        step = new_step;
        stepText.setText(String.valueOf(step));
    }
    private void updateGraph(double orientation , int step){
        graph1.removeAllSeries();
        series1.appendData(new DataPoint(series1.getHighestValueX()+1, orientation),true,1000);
        Log.d("app", "addData: " +graph1.getViewport().getMaxX(false));
        graph1.addSeries(series1);
        graph1.getViewport().setMaxX(series1.getHighestValueX());
        graph1.getViewport().setMinX(0);
        graph1.getViewport().setXAxisBoundsManual(true);
        graph2.removeAllSeries();
        series2.appendData(new DataPoint(series2.getHighestValueX()+1, step),true,100);
        graph2.addSeries(series2);
        graph2.getViewport().setMaxX(series2.getHighestValueX());
        graph2.getViewport().setMinX(0);
        graph2.getViewport().setXAxisBoundsManual(true);
    }
    private void sendDataToMQTT(String ID, String value){

        MqttMessage msg = new MqttMessage(); msg.setId(1234);
        msg.setQos(0); msg.setRetained(true);

        String data = ID + ":" + value;

        byte[] b = data.getBytes(Charset.forName("UTF-8"));
        msg.setPayload(b);

        try {
            mqttHelper.mqttAndroidClient.publish("youngboii/feeds/led", msg);
            mqttHelper.subscribeToTopic("youngboii/feeds/sensor-slash-light");

        }catch (MqttException e){
        }
    }
    private void startMQTT(){
        mqttHelper = new MQTTHelper(getApplicationContext());

        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
            }

            @Override
            public void connectionLost(Throwable throwable) {
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.d("app", "messageArrived: "+topic);
                try {
                        if (topic.equals("Watermelon_Fox/feeds/testing")){
                            JSONObject object= new JSONObject(new String(mqttMessage.getPayload()));
                            double compass =object.getDouble("compass");
                            int step =object.getInt("step");
                            Log.d("app", String.valueOf(heading));
                            Log.d("app",String.valueOf(compass));
                            updateDashboard(compass,step);
                            updateGraph(compass,step);
                        }
                } catch (Exception e){}
            }
            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        img= findViewById(R.id.compassVisual);
        graph1 = findViewById(R.id.compassGraph);
        graph2 = findViewById(R.id.stepGraph);
        compassText = findViewById(R.id.headingValue);
        graph1.getViewport().setMaxY(360);
        stepText = findViewById(R.id.stepCount);

        graph1.getViewport().setYAxisBoundsManual(true);
        graph2.getViewport().setMaxY(60);
        graph2.getViewport().setYAxisBoundsManual(true);
        startMQTT();

    }

}